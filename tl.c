/**************************************************************************************************
  Filename:       peripheral.c
  Revised:        $Date: 2015-01-23 11:01:40 -0800 (Fri, 23 Jan 2015) $
  Revision:       $Revision: 41985 $

  Description:    transport layer handling


  Copyright (c) 2015, Texas Instruments
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
  1. Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
  2. Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in the
     documentation and/or other materials provided with the distribution.
  3. All advertising materials mentioning features or use of this software
     must display the following acknowledgement:
     This product includes software developed by the Texas Instruments.
  4. Neither the name of the Texas Instruments nor the
     names of its contributors may be used to endorse or promote products
     derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY Texas Instruments ''AS IS'' AND ANY
  EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL Texas Instruments BE LIABLE FOR ANY
  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**************************************************************************************************/

/*********************************************************************
 * INCLUDES
 */
#include <ti/sysbios/knl/Semaphore.h>   
#include <ti/sysbios/family/arm/m3/Hwi.h>
#include <ti/sysbios/knl/Task.h>
   
#include "inc/npi_data.h"
#include "inc/npi_rxbuf.h"
#include "inc/npi_tl.h"

#include "tl.h"
   
/*********************************************************************
 * GLOBAL VARIABLES
 */

//event flags
uint16_t TX_DONE_ISR_EVENT_FLAG = 0;
uint16_t MRDY_ISR_EVENT_FLAG = 0;
uint16_t TRANSPORT_RX_ISR_EVENT_FLAG = 0;

/*********************************************************************
 * LOCAL VARIABLES
 */

//stores any TL-related events
static uint16_t TL_events = 0;

//event values passed in from app
static uint16_t transport_tx_done_event = 0;
static uint16_t transport_rx_event = 0;
static uint16_t mrdy_event = 0;

//pointer to application's semaphore
static ICall_Semaphore *appSem = NULL;
//pointer to app callback function
static TLCBs_t *TL_AppCBs = NULL;

/*********************************************************************
 * LOCAL FUNCTIONS
 */
static void transportTxDoneCallBack(int size);
static void transportRXCallBack(int size);
static void MRDYEventCB(int size);

/*********************************************************************
 *  FUNCTIONS DECLARATIONS
 */
// -----------------------------------------------------------------------------
//! \brief      Call back function for TX Done event from driver.
//!
//! \param[in]  size    Number of bytes transmitted.
//!
//! \return     void
// -----------------------------------------------------------------------------
static void transportTxDoneCallBack(int size)
{
  // Post the event to the task thread.
  TX_DONE_ISR_EVENT_FLAG = transport_tx_done_event;
  Semaphore_post(*appSem);
}

// -----------------------------------------------------------------------------
//! \brief      RX Callback provided to driver for RX Event (ie.Bytes
//!             received).
//!
//! \param[in]  size    Number of bytes received.
//!
//! \return     void
// -----------------------------------------------------------------------------
static void transportRXCallBack(int size)
{
  //move bytes from driver buffer to TL circular buffer
  NPIRxBuf_Read(size);
  // Post the event to the task thread
  TRANSPORT_RX_ISR_EVENT_FLAG = transport_rx_event;
  Semaphore_post(*appSem);
}

// -----------------------------------------------------------------------------
//! \brief      RX Callback provided to driver for MRDY Event
//!
//! \param[in]  size    N/A
//!
//! \return     void
// -----------------------------------------------------------------------------
static void MRDYEventCB(int size)
{
  //Post event to task thread
  MRDY_ISR_EVENT_FLAG = mrdy_event;
  Semaphore_post(*appSem);
}

/*********************************************************************
 * GLOBAL FUNCTIONS
 */
// -----------------------------------------------------------------------------
//! \brief      Perform any TL related processing of events
//!             posted from ISR's.
//!
//! \return     void
// -----------------------------------------------------------------------------
void TL_handleISRevent(void)
{
  // Capture the ISR events flags now within a critical section.  
  // We do this to avoid possible race conditions where the ISR is 
  // modifying the event mask while the task is read/writing it.
  UInt hwiKey = Hwi_disable(); 
  UInt taskKey = Task_disable();
  TL_events = TL_events | TX_DONE_ISR_EVENT_FLAG | 
    MRDY_ISR_EVENT_FLAG | TRANSPORT_RX_ISR_EVENT_FLAG;
  TX_DONE_ISR_EVENT_FLAG = 0;
  MRDY_ISR_EVENT_FLAG = 0;
  TRANSPORT_RX_ISR_EVENT_FLAG = 0;      
  Task_restore(taskKey);
  Hwi_restore(hwiKey);
  
  //handle driver events first to avoid race conditions
  //MRDY event
  if (TL_events & mrdy_event)
  {
    TL_events &= ~mrdy_event;
#ifdef POWER_SAVING
    //perform handshaking
    NPITL_handleMrdyEvent();
#endif //POWER_SAVING
  }
  
  // The Transport Layer has received some bytes
  if(TL_events & transport_rx_event)
  {
    //call application callback to parse data
    if ((TL_AppCBs) && (TL_AppCBs->pfnTLpacketparser))
    {
      TL_AppCBs->pfnTLpacketparser();
    }
    
    //continue reading if necessary
    if (NPIRxBuf_GetRxBufCount() == 0)
    {
      // No additional bytes to collect, clear the read flag.
      TL_events &= ~transport_rx_event;
    }
    else
    {
      // Additional bytes to collect, preserve the flag and repost
      // to the semaphore to read again
      Semaphore_post(*appSem);
    }
  }
  
  // The last transmission to the host has completed.
  if(TL_events & transport_tx_done_event)
  {
    // Current TX is done.
    TL_events &= ~transport_tx_done_event;
  }    
}

// -----------------------------------------------------------------------------
//! \brief      Initialize TL 
//!
//! \param[in]  pAppSem    pointer to semaphore used by the application for ICall
//! \param[in]  appCallbacks    pointer to app callback to parse received data
//! \param[in]  TX_DONE_EVENT   value of transmission done event
//! \param[in]  RX_EVENT   value of data received event
//! \param[in]  MRDY_EVENT   value of MRDY event
//!
//! \return     void
// -----------------------------------------------------------------------------
void TLinit(ICall_Semaphore *pAppSem, TLCBs_t *appCallbacks, uint16_t TX_DONE_EVENT,
            uint16_t RX_EVENT, uint16_t MRDY_EVENT)
{
  // Initialize Network Processor Interface (NPI) which in turn will init driver
  NPITL_initTL( &transportTxDoneCallBack,
               &transportRXCallBack,
               &MRDYEventCB );
  
  //store pointer to app's semaphore
  appSem = pAppSem;
  
  //store pointer to application's callback function
  TL_AppCBs = appCallbacks;
  
  //store event values passed from app so as not overlap app's events
  transport_tx_done_event = TX_DONE_EVENT;
  transport_rx_event = RX_EVENT;
  mrdy_event = MRDY_EVENT;
}

// -----------------------------------------------------------------------------
//! \brief      Wrapper to NPI write function
//!
//! \param[in]  buf    pointer to data to write
//! \param[in]  len    length of data to write
//!
//! \return     void
// -----------------------------------------------------------------------------
void TLwrite (uint8_t *buf, uint8_t len)
{
  NPITL_writeTL(buf, len);
}

// -----------------------------------------------------------------------------
//! \brief      Wrapper to NPI read function
//!
//! \param[in]  buf    pointer to place read data
//! \param[in]  len    length of data to read
//!
//! \return     void
// -----------------------------------------------------------------------------
void TLread (uint8_t *buf, uint8_t len)
{
  NPIRxBuf_ReadFromRxBuf(buf, len);
}

// -----------------------------------------------------------------------------
//! \brief      Wrapper to NPI GetRxBufLen function
//!
//! \return     uint16_t amount of bytes in the NPI circular buffer
// -----------------------------------------------------------------------------
uint16_t TLgetRxBufLen(void)
{
  return NPIRxBuf_GetRxBufCount();
}